# This file is a part of the HiRISE DTM Importer for Blender
#
# Copyright (C) 2017 Arizona Board of Regents on behalf of the Planetary Image
# Research Laboratory, Lunar and Planetary Laboratory at the University of
# Arizona.
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option)
# any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
# for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program.  If not, see <http://www.gnu.org/licenses/>.

"""A HiRISE DTM Importer for Blender"""

import bpy

from .ui import (
    ImportTerrainOperator,
    ReloadTerrainOperator,
    ReloadTerrainSettings,
    TerrainPanel,
)

bl_info = {
    "name": "HiRISE DTM Importer",
    "author": "Nicholas Wolf (nicwolf1@gmail.com)",
    "version": (0, 3, 1),
    "blender": (2, 80, 0),
    "license": "GPL",
    "location": "File > Import > HiRISE DTM (.img)",
    "description": "Import a HiRISE DTM as a mesh.",
    "warning": "May consume a lot of memory",
    "category": "Import-Export",
    "wiki_url": "",  # TBD
    "tracker_url": "",  # TBD
    "link": "",  # TBD
    "support": "TESTING",
}


classes = (
    ImportTerrainOperator,
    ReloadTerrainOperator,
    ReloadTerrainSettings,
    TerrainPanel,
)


def menu_import(self, context):
    self.layout.operator(
        ImportTerrainOperator.bl_idname, text=ImportTerrainOperator.bl_label
    )


def register():
    for cls in classes:
        bpy.utils.register_class(cls)
    bpy.types.Scene.HiriseDtmImporterSettings = bpy.props.PointerProperty(
        type=ReloadTerrainSettings
    )
    bpy.types.TOPBAR_MT_file_import.append(menu_import)


def unregister():
    for cls in reversed(classes):
        bpy.utils.unregister_class(cls)
    del bpy.types.Scene.HiriseDtmImporterSettings
    bpy.types.TOPBAR_MT_file_import.remove(menu_import)


if __name__ == "__main__":
    register()
