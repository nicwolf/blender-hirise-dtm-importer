.. _installation:

Installation
============

Installing the HiRISE DTM Importer add-on is simple and straightforward: 

1. If you haven't already, `download <https://gitlab.com/nicwolf/blender-hirise-dtm-importer/-/jobs/artifacts/master/raw/blender-hirise-dtm-importer.zip?job=build>`_
   the add-on.
  
2. Open Blender.

3. Navigate to *Edit > Preferences*.
   
   .. figure:: /_images/installation/user-preferences.jpg
      :align: center

4. Navigate to the *Add-ons* tab if it isn't already selected.

   .. figure:: /_images/installation/addons-tab.jpg
      :align: center

5. Select *Install...*.

   .. figure:: /_images/installation/install-from-file.jpg
      :align: center

6. Navigate to where you downloaded the add-on file and select it. Then click "Install Add-on".

   .. figure:: /_images/installation/install-addon.jpg
      :align: center

8. This should bring you back to the *Add-ons* tab in User Preferences. The
   DTM importer is registered as in testing so it won't be visible by default.
   Select *Testing* along the top of the add-ons browser. The add-on should be
   visible now, but grayed-out. To enable the add-on click the checkbox next to
   its name.

   .. figure:: /_images/installation/enable-addon.jpg
      :align: center

10. The add-on is now installed, continue on to :ref:`quickstart` to get an
    introduction to how to use it.