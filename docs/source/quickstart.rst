.. _quickstart:

Quickstart
==============================================================================

This guide will walk you through importing a HiRISE Digital Terrain Model
(DTM) into Blender. After completing this tutorial you will have imported a
HiRISE DTM into Blender, textured it with a photo of the Martian surface, and
rendered out a perspective view of the surface from a virtual camera.
Additionally, you'll learn how to handle all of the quirks of working with
our DTMs in Blender and should have no problem following other Blender
tutorials using our DTMs as assets.

1. If you haven't already, download a DTM. Visit the `HiRISE DTM catalogue
   <http://www.uahirise.org//dtm/>`_ and take your pick.

   .. figure:: /_images/quickstart/catalogue.jpg
      :align: center

   Our DTMs are big--usually around 250MB, so it's slow to preview them at
   full resolution. To get a good idea of what any DTM looks like, click on
   some of the products linked under "DTM EXTRAS". These are low-resolution
   images meant for fast previewing.

   .. figure:: /_images/quickstart/dtmextras.jpg
      :align: center

   Once you've found a DTM you like, right click on the first file under the
   "DTM & ORTHOIMAGES" section (it should start with ``DT``) and download the
   linked file.

   .. figure:: /_images/quickstart/download-dtm.jpg
      :align: center

   You'll also download a matching photo of the surface. These are called
   orthoimages, and are under the same column as the link to the DTM. Notice
   that some of the names have ``IRB`` in them and others have ``RED``
   --you'll want to download the ``RED`` images. Also notice that
   after the ``RED`` comes a single letter from the set ``{A, B, C, D}``--
   these indicate the resolution of the image, ``A`` being the highest and
   ``D`` being the lowest (More about naming scheme `here
   <http://www.uahirise.org//dtm/about.php>`_). For this tutorial, download
   any of the lower resolution ``RED`` images. The higher resolution ones
   work just the same (and look very nice) but will result in much longer
   load times. Your browser may try to open these images if you click on
   them, so the safest bet for downloading them is by right-clicking and
   selecting *Download* or *Save As...*

   .. figure:: /_images/quickstart/download-ortho.jpg
      :align: center

2. Open Blender.

3. Select *File > Import > Import HiRISE Terrain Model*.

   .. figure:: /_images/quickstart/import-menu.jpg
      :align: center

4. Navigate to where you downloaded the DTM. There are a handful of options
   in the right panel of the browser that configure the behavior of the importer
   (the default settings are the recommended settings):

   + **Terrain Model Resolution**: Determines how much of the data in the DTM
     end up as vertices in the 3D mesh in Blender. 100% means every data point
     in the original DTM will become a vertex, 10% means every 10th data point
     in the original DTM will become a vertex. Best practice is to leave this
     value at 10% (or even lower) when you first import a DTM, stage your
     scene, move the mesh and apply textures, etc... When you're ready to
     render, you can update the resolution of the terrain and all of the
     changes you've made to it will persist in the higher-resolution mesh
     (one caveat is that deletions from the mesh won't persist).

   + **Setup Blender Scene**: A HiRISE DTM is often a few kilometers long.
     Blender isn't used to dealing with objects that are this large and without
     making a few changes to the Blender viewport settings, the terrain may
     appear clipped or may not be visible at all. If this option is enabled the
     importer will handle these issues automatically by increasing the
     viewport clipping range and expanding the Blender grid floor so that grid
     lines are a kilometer apart.

   + **Set Blender Units to Meters**: By default, Blender's units are
     dimensionless. This option will set them to meters, which is useful for
     scale modeling on the terrain.

   Select the DTM you downloaded and click *Import HiRISE Terrain Model*. It
   may take a few minutes (or even longer, if you increased the terrain
   resolution) to load the DTM and create a mesh from it.

   .. figure:: /_images/quickstart/navigate-to-dtm.jpg
      :align: center

5. You should end up back in the Blender viewport, with a little piece of
   Mars in front of you!

   .. figure:: /_images/quickstart/birdseye.jpg
      :align: center

6. Next, we'll drape the photo we downloaded over the terrain as a
   texture. Make sure the "Terrain" object is selected and switch to the
   Materials tab. Create a new material.

   .. figure:: /_images/quickstart/new-material.jpg
      :align: center

   .. warning::
      
      The way that we set the scene up in this guide kind of assumes that the
      only thing in your scene is going to be the HiRISE DTM. By now you've
      seen everything HiRISE-specific about working with this add-on. If you'd
      like to render more complex scenes, you'd be better served by
      consulting any of the general Blender tutorial content out there in the
      wide, digital world.

7. Set the surface type to "Emission".

   .. figure:: /_images/quickstart/material-settings.jpg
      :align: center

8. Click the "Color" field and select "Image Texture".

   .. figure:: /_images/quickstart/material-settings-color.jpg
      :align: center

9. Click the "Open" button.

   .. figure:: /_images/quickstart/material-settings-color-open.jpg
      :align: center

9. Select the orthoimage that you downloaded in step 1 and click "Open Image".

   .. figure:: /_images/quickstart/material-settings-color-image.jpg
      :align: center

12. Check that everything is working by changing the viewport shading to "Material Preview" mode. Nice!

   .. figure:: /_images/quickstart/switch-to-render.jpg
      :align: center

13. Select the camera panel. Change the transform settings so that it is 500m
    above the origin, and then click the "Camera View" button:

   .. figure:: /_images/quickstart/goto-camera.jpg
      :align: center

   If you end up with a blank screen, don't worry!, that's expected:

   .. figure:: /_images/quickstart/camera-view-clipped.jpg
      :align: center

16. The problem is, again, that Blender isn't used to working with such large
    objects and by default, Blender cameras don't see anything closer than 10cm
    or further away than 100m. We can change this by increasing our camera's
    clipping range. Select your camera object, switch to the camera panel and
    increase the "End" Clipping distance to something like 10km. Now your
    viewport should properly show the terrain:

   .. figure:: /_images/quickstart/fix-camera-clip.jpg
      :align: center

17. Select *Render > Render Image*.

   .. figure:: /_images/quickstart/render-tab.jpg
      :align: center

   It may take a couple of minutes to render, but when it's done you should
   see a nice 3D view of the surface. If you want to hold on to this image,
   select *Image > Save As* and save it somewhere.